import 'package:cat_fact/core/extensions/list_extensions.dart';
import 'package:cat_fact/core/providers/shape_provider/shape_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';

enum Shapes { PointsClipper, DiagonalPathClipper }

class CatShapeProvider extends ShapeProvider {
  @override
  CustomClipper getClipper() {
    final _shapeType = Shapes.values.getRandom();

    CustomClipper _clipper;

    switch (_shapeType) {
      case Shapes.PointsClipper:
        _clipper = PointsClipper();
        break;
      case Shapes.DiagonalPathClipper:
        _clipper = DiagonalPathClipperOne();
        break;
    }

    return _clipper;
  }
}
