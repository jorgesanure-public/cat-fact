extension ListExtensions on List {
  getRandom() => ([...this]..shuffle()).first;
}
