import 'package:flutter/material.dart';

abstract class IImageRepository {
  Widget getImageWidget({Function(ImageProvider) imageProviderBuilder});
}
