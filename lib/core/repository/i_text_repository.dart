import 'package:flutter/material.dart';

abstract class ITextRepository {
  Future<String> getText();
}
