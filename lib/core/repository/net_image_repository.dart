import 'package:cached_network_image/cached_network_image.dart';
import 'package:cat_fact/core/repository/i_image_repository.dart';
import 'package:cat_fact/core/utils/app_utils.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math; // import this
import 'package:cat_fact/core/extensions/list_extensions.dart';

abstract class NetImageRepository implements IImageRepository {
  double get loadingImageOpacity => 1;

  bool _loadingImageDirection;

  /**
   * Get image widget
   */
  Widget getImageWidget({Function(ImageProvider) imageProviderBuilder}) {
    _loadingImageDirection = math.Random().nextBool();
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: _imageUrlBuilder(
          builder: (imageUrl) => CachedNetworkImage(
              imageUrl: imageUrl,
              fit: BoxFit.contain,
              placeholder: (context, url) => _getLoadingImage(),
              errorWidget: (context, url, error) => _getLostImage(),
              imageBuilder: (context, imageProvider) {
                imageProviderBuilder?.call(imageProvider);
                return Container(
                    color: Colors.black12,
                    child: Image(
                      image: imageProvider,
                      fit: BoxFit.contain,
                    ));
              })),
    );
  }

  /** 
   * Get image to show when other image not found
   */
  String getLostImageFromAsset();
  Widget _getLostImage() => _getImage(getLostImageFromAsset(), expanded: true);

  /** 
   * Get image to show while loading other image
   */
  String getLoadingImageFromAsset();
  Widget _getLoadingImage() {
    Widget _result = _getImage(getLoadingImageFromAsset(), height: 120);

    if (_loadingImageDirection) {
      _result = Transform(
        alignment: Alignment.center,
        transform: Matrix4.rotationY(math.pi),
        child: _result,
      );
    }
    _loadingImageDirection = !_loadingImageDirection;

    return Opacity(opacity: loadingImageOpacity, child: _result);
  }

  // Get asset image widget
  Widget _getImage(String imageAssetName,
      {bool expanded = false, double height}) {
    Widget widget = ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Image.asset(
          imageAssetName,
          fit: BoxFit.contain,
          height: height,
        ));

    if (expanded)
      widget = Expanded(
        child: widget,
      );

    return imageAssetName == null
        ? SizedBox.shrink()
        : Container(
            color: Colors.black12,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                widget,
              ],
            ),
          );
  }

  /**
   * Get image url
   */
  String get _imageUrl => _getApiUrl() + AppUtils.getRandomNumber();

  Future<String> Function() get _imageUrlFromBuilder =>
      apiUrlBuilders.getRandom();

  String _getApiUrl() => apiUrls?.getRandom();

  List<String> get apiUrls;

  List<Future<String> Function()> get apiUrlBuilders => null;

  Widget _imageUrlBuilder({Widget Function(String) builder}) {
    // Decide if taking an api url from builders or not
    bool sourceImageFromBuilder = (apiUrlBuilders?.isNotEmpty ?? false)
        ? false
        : math.Random().nextBool();

    Widget result;

    if (sourceImageFromBuilder) {
      result = builder(_imageUrl);
    } else {
      result = FutureBuilder(
        future: _imageUrlFromBuilder.call(),
        builder: (context, snapshot) {
          Widget result;
          if (snapshot.connectionState == ConnectionState.waiting)
            result = SizedBox.shrink();
          else
            result = snapshot.data != null
                ? builder(snapshot.data)
                : builder(_imageUrl);
          return result;
        },
      );
    }

    return result;
  }
}
