import 'dart:math';

class AppUtils {
  AppUtils._();

  /**
   * Get a random number
   */
  static String getRandomNumber({
    // Desired number length to be generated
    int numLen: 20,
  }) {
    int len = Random().nextInt(numLen) + 1;
    String number = '';
    for (int i = 0; i < len; i++) number += Random().nextInt(9).toString();
    return number;
  }
}
