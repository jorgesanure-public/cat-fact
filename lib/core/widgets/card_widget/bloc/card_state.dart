import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:flutter/material.dart';

part 'card_state.g.dart';

@CopyWith()
class CardState {
  final bool isLoadingImage;
  final bool isLoadingText;
  final ImageProvider imageProvider;
  final Widget image;
  final Widget text;

  CardState(
      {this.text,
      this.image,
      this.imageProvider,
      this.isLoadingImage = false,
      this.isLoadingText = false});

  bool get isLoadingContent => isLoadingText || isLoadingImage;
}
