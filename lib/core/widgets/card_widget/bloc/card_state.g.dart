// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'card_state.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension CardStateCopyWith on CardState {
  CardState copyWith({
    Widget image,
    ImageProvider<dynamic> imageProvider,
    bool isLoadingImage,
    bool isLoadingText,
    Widget text,
  }) {
    return CardState(
      image: image ?? this.image,
      imageProvider: imageProvider ?? this.imageProvider,
      isLoadingImage: isLoadingImage ?? this.isLoadingImage,
      isLoadingText: isLoadingText ?? this.isLoadingText,
      text: text ?? this.text,
    );
  }
}
