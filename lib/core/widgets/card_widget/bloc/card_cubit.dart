import 'package:cat_fact/core/providers/font_provider/font_provider.dart';
import 'package:cat_fact/core/providers/font_provider/i_font_provider.dart';
import 'package:cat_fact/core/providers/shape_provider/i_shape_provider.dart';
import 'package:cat_fact/core/providers/shape_provider/shape_provider.dart';
import 'package:cat_fact/core/repository/i_image_repository.dart';
import 'package:cat_fact/core/repository/i_text_repository.dart';
import 'package:cat_fact/core/widgets/card_widget/bloc/card_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CardCubit extends Cubit<CardState> {
  final IImageRepository imageRepository;
  final ITextRepository textRepository;
  IShapeProvider shapeProvider;
  IFontProvider fontProvider;

  CardCubit(
      {this.fontProvider,
      CardState state,
      this.shapeProvider,
      @required this.imageRepository,
      @required this.textRepository})
      : super(state) {
    assert(imageRepository != null);
    assert(textRepository != null);

    shapeProvider ??= ShapeProvider();
    fontProvider ??= FontProvider();
  }

  void loadNewContent() {
    _loadNewImage();
    _loadNewText();
  }

  void _loadNewText() async {
    emit(state.copyWith(isLoadingText: true));

    final _text = await textRepository.getText();
    final _widget = _text == null ? null : fontProvider.create(text: _text);

    emit(state.copyWith(isLoadingText: false, text: _widget));
  }

  void _loadNewImage() {
    emit(state.copyWith(isLoadingImage: true));

    Widget image = imageRepository.getImageWidget(
      imageProviderBuilder: (imageProvider) {
        emit(state.copyWith(imageProvider: imageProvider));
        Future.delayed(Duration(milliseconds: 500))
            .then((_) => emit(state.copyWith(isLoadingImage: false)));
      },
    );

    image = _createShape(image);

    emit(state.copyWith(image: image));
  }

  Widget _createShape(Widget child) =>
      shapeProvider?.create(child: child) ?? child;
}
