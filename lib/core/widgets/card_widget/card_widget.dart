import 'package:cat_fact/core/providers/font_provider/i_font_provider.dart';
import 'package:cat_fact/core/providers/shape_provider/i_shape_provider.dart';
import 'package:cat_fact/core/repository/i_image_repository.dart';
import 'package:cat_fact/core/repository/i_text_repository.dart';
import 'package:cat_fact/core/widgets/background_color_by_image_widget.dart';
import 'package:cat_fact/core/widgets/card_widget/bloc/card_cubit.dart';
import 'package:cat_fact/core/widgets/card_widget/bloc/card_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:over_screen_widget/controllers/i_over_screen_controller.dart';
import 'package:over_screen_widget/controllers/over_screen_controller.dart';
import 'package:over_screen_widget/over_screen_widget.dart';

class CardWidget extends StatefulWidget {
  final IImageRepository imageRepository;
  final ITextRepository textRepository;
  final IShapeProvider shapeProvider;
  final IFontProvider fontProvider;
  final OverScreenWidget Function(Widget, Function(OverScreenController))
      overScreenBuilder;
  final Widget Function(Widget image, Widget text) contentBuilder;

  CardWidget(
      {Key key,
      @required this.imageRepository,
      @required this.textRepository,
      this.shapeProvider,
      this.overScreenBuilder,
      this.fontProvider,
      this.contentBuilder})
      : super(key: key) {
    assert(imageRepository != null, 'imageRepository shouldn\'t be null');
    assert(textRepository != null, 'textRepository shouldn\'t be null');
  }

  @override
  _CardWidgetState createState() => _CardWidgetState();
}

class _CardWidgetState extends State<CardWidget> {
  IOverScreenController _overScreenController;
  OverScreenWidget Function(Widget, Function(OverScreenController))
      overScreenBuilder;
  CardCubit _cubit;
  Size _size;

  @override
  void initState() {
    super.initState();
    overScreenBuilder = widget.overScreenBuilder ??
        (child, controllerBuilder) => OverScreenWidget(
              builder: (controller) {
                controllerBuilder(controller);
                return child;
              },
              keepChildVisible: true,
              overScreenContent: SizedBox.shrink(),
            );
    _cubit = CardCubit(
        imageRepository: widget.imageRepository,
        textRepository: widget.textRepository,
        shapeProvider: widget.shapeProvider,
        fontProvider: widget.fontProvider,
        state: CardState(
          isLoadingImage: false,
          isLoadingText: false,
        ));

    WidgetsBinding.instance
        .addPostFrameCallback((_) => _cubit.loadNewContent());
  }

  @override
  Widget build(BuildContext context) {
    _size = MediaQuery.of(context).size;

    return Scaffold(
      // To use with download and other long tasks. Cover whole screen
      body: Builder(
        builder: (context) {
          return BlocListener<CardCubit, CardState>(
              cubit: _cubit,
              listener: _listenCardCubitStateChanges,
              child: overScreenBuilder.call(_createUnderScreenChild(context),
                  (controller) => _overScreenController = controller));
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.amber,
        onPressed: _cubit.loadNewContent,
        child: Icon(
          Icons.refresh,
          color: Colors.white,
          size: 45,
        ),
      ),
    );
  }

  /**
   * Under screen child
   */
  Widget _createUnderScreenChild(BuildContext context) => InkWell(
        onTap: _cubit.loadNewContent,
        child: Stack(
          alignment: Alignment.topCenter,
          children: [
            BlocBuilder<CardCubit, CardState>(
              cubit: _cubit,
              buildWhen: (previous, current) =>
                  previous.imageProvider != current.imageProvider,
              builder: (context, state) => BackgroundColorByImageWidget(
                  imageProvider: state.imageProvider),
            ),
            _createContentPage()
          ],
        ),
      );

  /**
   * Build image widget
   */
  Widget _createImage() => BlocBuilder<CardCubit, CardState>(
      cubit: _cubit,
      builder: (context, state) => ConstrainedBox(
            constraints: BoxConstraints(
                minHeight: 0,
                minWidth: _size.width * 0.95,
                maxHeight: _size.height * 0.7),
            child: state.image,
          ));

  /**
   * Create text widget
   */
  Widget _createText() => BlocBuilder<CardCubit, CardState>(
        cubit: _cubit,
        builder: (context, state) => Container(
            alignment: Alignment.center,
            margin: EdgeInsets.symmetric(horizontal: 9),
            padding: EdgeInsets.symmetric(horizontal: 14, vertical: 10),
            decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.8),
                borderRadius: BorderRadius.circular(8)),
            child: state.text),
      );

  Widget _createContentPage() {
    Widget _widget =
        widget.contentBuilder?.call(_createImage(), _createText()) ??
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _createImage(),
                SizedBox(
                  height: 15,
                ),
                _createText()
              ],
            );

    return SingleChildScrollView(
      child: ConstrainedBox(
        constraints: BoxConstraints(minHeight: _size.height),
        child: _widget,
      ),
    );
  }

  void _listenCardCubitStateChanges(BuildContext context, CardState state) {
    if (state.isLoadingContent)
      _overScreenController.show();
    else
      _overScreenController.hide();
  }
}
