import 'package:flutter/material.dart';
import 'package:palette_generator/palette_generator.dart';

class BackgroundColorByImageWidget extends StatefulWidget {
  final ImageProvider imageProvider;

  const BackgroundColorByImageWidget({Key key, this.imageProvider})
      : super(key: key);

  @override
  _BackgroundColorByImageWidgetState createState() =>
      _BackgroundColorByImageWidgetState();
}

class _BackgroundColorByImageWidgetState
    extends State<BackgroundColorByImageWidget> {
  @override
  Widget build(BuildContext context) => FutureBuilder(
        future: _createColorsFromImage(imageProvider: widget.imageProvider),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) =>
            _createBackground(colors: snapshot.data),
      );

  Widget _createBackground({List<Color> colors}) {
    final _defaultColors = [Colors.grey.withOpacity(0.01), Colors.black];
    final _colors = (colors?.length ?? 0) < 2 ? _defaultColors : colors;

    return Opacity(
      opacity: 0.5,
      child: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: _colors)),
      ),
    );
  }

  Future<dynamic> _createColorsFromImage({imageProvider}) =>
      PaletteGenerator.fromImageProvider(
        imageProvider,
        // size: widget.imageSize,
        // region: newRegion,
        maximumColorCount: 5,
      ).then((generator) => generator.colors.toList());
}
