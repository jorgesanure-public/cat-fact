import 'package:flutter/material.dart';

class ScrollableWidget extends StatefulWidget {
  final Widget child;
  final Function onDragUpAtTop;
  final Function onDragDownAtBottom;

  ScrollableWidget(
      {Key key, this.child, this.onDragUpAtTop, this.onDragDownAtBottom})
      : super(key: key);

  @override
  _ScrollableWidgetState createState() => _ScrollableWidgetState();
}

class _ScrollableWidgetState extends State<ScrollableWidget> {
  final ScrollController _controller = ScrollController();

  bool isAtTop = true;
  bool isAtBottom = false;

  @override
  void initState() {
    super.initState();

    _controller.addListener(() {
      final _contentIsHigherThanScreen =
          _controller.position.maxScrollExtent != 6.0;

      if (_controller.position.atEdge) {
        print('--------------->');
        if (_controller.position.pixels == 0) {
          // you are at top position

          if (_contentIsHigherThanScreen && !isAtTop) {
            isAtTop = true;
            isAtBottom = false;
          } else {
            widget.onDragUpAtTop?.call();
          }
          _scrollOneDown();
        } else {
          // you are at bottom position

          if (_contentIsHigherThanScreen && !isAtBottom) {
            isAtBottom = true;
            isAtTop = false;
          } else {
            widget.onDragDownAtBottom?.call();
          }
          _scrollOneUp();
        }
      } else if (_controller.position.pixels > 1 &&
          _controller.position.pixels <
              (_controller.position.maxScrollExtent - 1)) {
        isAtTop = false;
        isAtBottom = false;
      }
    });

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _scrollOneDown();
    });
  }

  @override
  Widget build(BuildContext context) => SingleChildScrollView(
        controller: _controller,
        child: Column(
          children: [
            SizedBox(
              height: 1,
            ),
            widget.child,
          ],
        ),
      );

  void _scrollOneDown() => _controller.animateTo(1,
      duration: Duration(milliseconds: 300), curve: Curves.easeIn);

  void _scrollOneUp() =>
      _controller.animateTo(_controller.position.maxScrollExtent - 1,
          duration: Duration(milliseconds: 300), curve: Curves.easeIn);
}
