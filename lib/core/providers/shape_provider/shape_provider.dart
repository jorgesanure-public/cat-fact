import 'package:cat_fact/core/providers/shape_provider/i_shape_provider.dart';
import 'package:cat_fact/core/extensions/list_extensions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';

enum Shapes { OvalBottomBorderClipper, DiagonalPathClipper }

class ShapeProvider extends IShapeProvider {
  @override
  CustomClipper getClipper() {
    final _shapeType = Shapes.values.getRandom();

    CustomClipper _clipper;

    switch (_shapeType) {
      case Shapes.OvalBottomBorderClipper:
        _clipper = OvalBottomBorderClipper();
        break;
      case Shapes.DiagonalPathClipper:
        _clipper = DiagonalPathClipperOne();
        break;
    }

    return _clipper;
  }
}
