import 'package:flutter/material.dart';

abstract class IShapeProvider {
  CustomClipper getClipper();

  Widget create({Widget child}) => ClipPath(
        clipper: getClipper(),
        child: child,
      );
}
