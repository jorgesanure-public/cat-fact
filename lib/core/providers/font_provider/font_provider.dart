import 'package:cat_fact/core/providers/font_provider/i_font_provider.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class FontProvider extends IFontProvider {
  List<TextStyle> get fontList => _fontList;

  List<TextStyle> _fontList = [
    GoogleFonts.openSans(),
    GoogleFonts.montserrat(),
    GoogleFonts.poppins()
  ];
}
