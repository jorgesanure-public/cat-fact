import 'package:cat_fact/core/extensions/list_extensions.dart';
import 'package:flutter/material.dart';

abstract class IFontProvider {
  Widget create({String text, Function(TextStyle) customTextStyleBuilder}) {
    TextStyle _style = fontList.getRandom().copyWith(fontSize: 18.0);

    _style = customTextStyleBuilder?.call(_style) ?? _style;

    return Text(
      text,
      style: _style,
    );
  }

  List<TextStyle> get fontList;
}
