import 'dart:convert';

import 'package:http/http.dart' as http;

class RandomCatApiService {
  static final String _baseUrl = 'http://aws.random.cat/meow';

  static Future<String> _getImageUrl() async {
    String imageUrl;

    // Catch any error from http request
    try {
      final responseBody = await _getImageUrlFromAPI();
      imageUrl = ImageUrl.fromHttpResponse(responseBody).toString();
    } catch (e) {}

    return imageUrl;
  }

  static Future<String> _getImageUrlFromAPI() async =>
      (await http.get(_baseUrl)).body;

  static Future<String> getImageUrl() => _getImageUrl();
}

class ImageUrl {
  String url;

  ImageUrl.fromHttpResponse(String response) {
    var data = json.decode(response);
    this.url = data['file'];
  }

  String toString() => this.url;
}
