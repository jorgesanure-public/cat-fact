import 'package:cat_fact/constants/app_images.dart';
import 'package:cat_fact/core/repository/net_image_repository.dart';
import 'package:cat_fact/services/cat_image_service/the_cat_api_service.dart';

import 'random_cat_api_service.dart';

class CatImageService extends NetImageRepository {
  final String cataasApi = 'https://cataas.com/cat?';
  final String randomkittengeneratorApi =
      'http://www.randomkittengenerator.com/cats/rotator.php?';

  @override
  List<String> get apiUrls => [cataasApi, randomkittengeneratorApi];

  @override
  List<Future<String> Function()> get apiUrlBuilders =>
      [TheCatApiService.getImageUrl, RandomCatApiService.getImageUrl];

  @override
  String getLoadingImageFromAsset() => AppImages.running_cat;

  @override
  String getLostImageFromAsset() => AppImages.lost_cat;

  @override
  double get loadingImageOpacity => 0.3;
}
