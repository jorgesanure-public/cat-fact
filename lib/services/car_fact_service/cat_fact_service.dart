import 'dart:convert';

import 'package:cat_fact/core/repository/i_text_repository.dart';
import 'package:http/http.dart' as http;

class CatFactService implements ITextRepository {
  static final String factUrl = 'https://meowfacts.herokuapp.com/';

  static Future<String> _getCatFact() async {
    String fact = '';

    // Catch any error from http request
    try {
      final responseBody = await getCatFactFromAPI();
      fact = Fact.fromHttpResponse(responseBody).toString();

      final _lowercaseFact = fact.toLowerCase();
      if (_lowercaseFact.indexOf('invalid command') != -1) fact = null;
      if (_lowercaseFact.indexOf('catfacts') != -1) fact = null;
    } catch (e) {
      fact = 'Cat fact not found (Check your cat internet)';
    }
    return fact;
  }

  static Future<String> getCatFactFromAPI() async =>
      (await http.get(factUrl)).body;

  @override
  Future<String> getText() => _getCatFact();
}

class Fact {
  String fact;

  Fact.fromHttpResponse(String response) {
    var data = json.decode(response);
    this.fact = data['data'][0];
  }

  @override
  String toString() => fact;
}
