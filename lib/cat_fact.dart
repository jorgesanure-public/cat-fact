import 'package:cat_fact/core/widgets/card_widget/card_widget.dart';
import 'package:cat_fact/providers/cat_shape_provider.dart';
import 'package:cat_fact/services/car_fact_service/cat_fact_service.dart';
import 'package:cat_fact/services/cat_image_service/cat_image_service.dart';
import 'package:flutter/material.dart';

class CatFact extends CardWidget {
  CatFact()
      : super(
          imageRepository: CatImageService(),
          textRepository: CatFactService(),
          shapeProvider: CatShapeProvider(),
          contentBuilder: (image, text) => Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              image,
              SizedBox(
                height: 15,
              ),
              text,
              Container(
                  margin: EdgeInsets.symmetric(vertical: 20),
                  child: Text(
                    '(◠﹏◠)\t (づ￣ ³￣)づ\t♥‿♥',
                    style: TextStyle(fontSize: 22),
                  ))
            ],
          ),
          // overScreenBuilder: (child, controllerBuilder) => OverScreenWidget(
          //       keepChildVisible: true,
          //       overScreenContent: Container(
          //           alignment: Alignment.center,
          //           decoration: BoxDecoration(
          //               borderRadius: BorderRadius.circular(20),
          //               color: Colors.white.withOpacity(0.8)),
          //           child: ClipRRect(
          //             borderRadius: BorderRadius.circular(20),
          //             child: Image.asset(AppImages.running_cat),
          //           )),
          //       builder: (controller) {
          //         controllerBuilder(controller);
          //         return child;
          //       },
          //     )
        );
}
