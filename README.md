# Cat Facts

A Flutter project showing a random cat picture and facts.

<!-- <img src="images/preview.gif" height='300px'> -->

## Todo

TODA LOGICA EN CUBIT, QUITAR STREAMBUILDERS
SE ESTA USANDO UN MISMO STREAMBUILDER STREAM VARIAS VECES MAAAAL, SE HA DE CONVINAR MULTIPLES STREAM
SE QUIERE ASEGURAR CARGA DE IMAGEN Y TEXTO JUNTOS ANTES DE MOSTRAR
SE MOSTRARA SIEMPRE FONDO

- [x] Generate background color from image colors
- [ ] Add to Favorites
- [ ] Share > screenshot + 'I Love Cats! #catfact link_to_play_store'
- [ ] Shuffle background colors
- [ ] Shuffle image shape
- [ ] Shuffle fact
- [ ] Add to Play Store (Google)
- [ ] Add to App Store (Apple IOs)
- [ ] Add drawer: author image, author name, author email, about me, credits, app info, how to, 
- [ ] App Icon
- [ ] Splashscreen
- [ ] About App
- [ ] Side menu
- [ ] Bottom nav bar

## Attributions

### Mobile Application Icon: 
- "Icon made by Freepik perfect from [www.flaticon.com](www.flaticon.com)"

### Cat Loading Image
- [Giphy](https://giphy.com/gifs/hoppip-cat-animation-hoppip-O7hIMjgr0dTig)

### Cat pictures 
- [Cataas](https://cataas.com)
- [The Cat Api](https://api.thecatapi.com)
- [AWS Random Cat](http://aws.random.cat)
- [Random Kitten Generator](http://www.randomkittengenerator.com)
- [Cataas](https://cataas.com)

### Cat facts
- [Meowfacts](https://meowfacts.herokuapp.com/)
